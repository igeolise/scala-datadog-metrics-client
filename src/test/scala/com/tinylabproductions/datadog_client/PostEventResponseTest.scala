package com.tinylabproductions.datadog_client

import org.scalatest.{FunSpec, Matchers}
import play.api.libs.json.Json

/**
 * Created with IntelliJ IDEA.
 * User: martynas
 * Date: 11/19/13
 * Time: 2:43 PM
 * To change this template use File | Settings | File Templates.
 */
class PostEventResponseTest extends FunSpec with Matchers {

  implicit val format = PostEventResponse.readFormat

  describe("PostEventResponse") {
    it("should parse valid json") {
      val json = Json.stringify(Json.obj("event" -> Json.obj(
        "url" -> "url",
        "tags" -> Json.arr("tag1", "host:tag2")
      )))

      Json.parse(json).as[PostEventResponse]
        === (PostEventResponse(Some("url"), Some("tag2")))
    }

    it("should ignore missing url field") {
      val json = Json.stringify(Json.obj("event" -> Json.obj(
        "tags" -> Json.arr("tag1", "host:tag2")
      )))

      Json.parse(json).as[PostEventResponse]
      === (PostEventResponse(None, Some("tag2")))
    }

    it("should ignore missing tags field") {
      val json = Json.stringify(Json.obj("event" -> Json.obj(
        "url" -> "url"
      )))

      Json.parse(json).as[PostEventResponse]
        === (PostEventResponse(Some("url"), None))
    }

    it("should ignore missing tag") {
      val json = Json.stringify(Json.obj("event" -> Json.obj(
        "url" -> "url",
        "tags" -> Json.arr("tag1")
      )))

      Json.parse(json).as[PostEventResponse]
        === (PostEventResponse(Some("url"), None))
    }
  }
}
