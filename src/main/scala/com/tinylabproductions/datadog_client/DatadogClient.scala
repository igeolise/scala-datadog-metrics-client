package com.tinylabproductions.datadog_client

import com.timgroup.statsd.NonBlockingStatsDClient
import java.util.Date
import dispatch._
import play.api.libs.json.{JsValue, Json}
import com.ning.http.client.RequestBuilder

sealed trait Priority { val name: String }
object Priority {
  case object Normal extends Priority { val name = "normal" }
  case object Low extends Priority { val name = "low" }
}

sealed trait AlertType { val name: String }
object AlertType {
  case object Error extends AlertType { val name = "error" }
  case object Warning extends AlertType { val name = "warning" }
  case object Info extends AlertType { val name = "info" }
  case object Success extends AlertType { val name = "success" }
}

sealed trait SourceType { val name: String }
object SourceType {
  case object Nagios extends SourceType { val name = "nagios" }
  case object Hudson extends SourceType { val name = "hudson" }
  case object Jenkins extends SourceType { val name = "jenkins" }
  case object User extends SourceType { val name = "user" }
  case object MyApps extends SourceType { val name = "my apps" }
  case object Feed extends SourceType { val name = "feed" }
  case object Chef extends SourceType { val name = "chef" }
  case object Puppet extends SourceType { val name = "puppet" }
  case object Git extends SourceType { val name = "git" }
  case object BitBucket extends SourceType { val name = "bitbucket" }
  case object Fabric extends SourceType { val name = "fabric" }
  case object Capistrano extends SourceType { val name = "capistrano" }
}

case class StatsDEndpoint(
  prefix: String, hostname: String, port: Int, tags: Seq[String]
)
case class ApiEndpoint(
  apiKey: String, url: String="https://app.datadoghq.com/api/v1"
)

/**
 * Represents a response after posting an event. Is passed to the callback
 * function.
 *
 * @param eventUrl
 * @param host
 */
case class PostEventResponse(
  eventUrl: Option[String], host: Option[String]
)

object PostEventResponse {
  import play.api.libs.json._
  import play.api.libs.functional.syntax._

  /**
   * Valid json:
   * {
   *   ...
   *   "event": {
   *     ...
   *     "url": "...",
   *     "tags": ["...", "host:..."],
   *   }
   * }
   */
  val readFormat = (
    (__ \ "event" \ "url").readNullable[String] ~
    (__ \ "event" \ "tags").readNullable[Array[String]].map { optionalArray =>
      optionalArray.flatMap { array =>
        array.filter(_.contains("host:")).headOption.flatMap { string =>
          Some(string.replace("host:", ""))
        }
      }
    }
  )(PostEventResponse.apply _)
}

object DatadogClient {
  implicit class RequestBuilderExts(val rb: RequestBuilder) extends AnyVal {
    def postJson(js: JsValue) = {
      rb.POST.addHeader("Content-Type", "application/json") << js.toString()
    }
  }
}

trait IDatadogClient {
  /**
   * Post an Event
   *
   * Allows you to post events to the stream. You can tag them, set priority
   * and aggregate them with other events.
   *
   * @param title The event title.
   * @param text The body of the event.
   * @param happenedAt Timestamp of the event. Now by default.
   * @param priority The priority of the event.
   * @param tags A list of tags to apply to the event.
   * @param alertType
   * @param aggregationKey An arbitrary string to use for aggregation.
   * @param sourceType The type of event being posted.
   */
  def event(
    title: String, text: String,
    happenedAt: Option[Date]=None,
    priority: Priority=Priority.Normal,
    tags: Seq[String]=Seq.empty,
    alertType: AlertType=AlertType.Info,
    aggregationKey: Option[String]=None,
    sourceType: Option[SourceType]=None,
    onSuccess: Option[PostEventResponse => Unit]=None
  )
  (implicit log: Logger)

  /**
   * Records execution time of given block
   */
  def timed[T](aspect: String, tags: String*)(block: => T)
  (implicit log: Logger): T

  /**
   * Records execution time for given aspect.
   */
  def recordExecutionTime(aspect: String, timeInMs: Long, tags: String*)
  (implicit log: Logger)

  /**
   * Records execution time for given aspects.
   */
  def recordExecutionTime(aspects: Seq[String], timeInMs: Long, tags: String*)
  (implicit log: Logger)

  /**
   * Increases/decreases counter.
   */
  def counter(aspect: String, delta: Int=1, tags: Seq[String]=Seq.empty)
  (implicit log: Logger)
}

object NoOpDatadogClient extends IDatadogClient {
  def event(
    title: String, text: String, happenedAt: Option[Date], priority: Priority,
    tags: Seq[String], alertType: AlertType, aggregationKey: Option[String],
    sourceType: Option[SourceType], onSuccess: Option[PostEventResponse => Unit]
  ) (implicit log: Logger) {}

  def timed[T](aspect: String, tags: String*)(block: => T)
  (implicit log: Logger) = block

  def recordExecutionTime(aspect: String, timeInMs: Long, tags: String*)
  (implicit log: Logger) {}

  def recordExecutionTime(aspects: Seq[String], timeInMs: Long, tags: String*)
  (implicit log: Logger) {}

  def counter(aspect: String, delta: Int, tags: Seq[String])
  (implicit log: Logger) {}
}

class DatadogClient(
  statsEP: StatsDEndpoint, apiEP: ApiEndpoint
)
extends IDatadogClient {
  import DatadogClient._

  private[this] val stats = new NonBlockingStatsDClient(
    statsEP.prefix, statsEP.hostname, statsEP.port,
    statsEP.tags.toArray
  )

  def event(
    title: String, text: String,
    happenedAt: Option[Date],
    priority: Priority,
    tags: Seq[String],
    alertType: AlertType,
    aggregationKey: Option[String],
    sourceType: Option[SourceType],
    onSuccess: Option[PostEventResponse => Unit]
  )
  (implicit log: Logger) {
    type KV = (String, Json.JsValueWrapper)
    def optField[T](opt: Option[T])(f: T => KV) = opt.map(value => f(value))

    val fields = Seq.apply[KV](
      "title" -> title,
      "text" -> text,
      "priority" -> priority.name,
      "tags" -> (tags ++ statsEP.tags).map(Json.toJson(_)),
      "alert_type" -> alertType.name
    ) ++
      optField(happenedAt)("date_happened" -> _.getTime) ++
      optField(aggregationKey)("aggregation_key" -> _) ++
      optField(sourceType)("source_type_name" -> _.name)

    val data = Json.obj(fields:_*)
    log.debug(s"Posting event: $data")

    Http(apiUrl("events") postJson data).onComplete {
      case Right(response) =>
        log.debug(s"Success after posting event: ${response.getResponseBody}")
        onSuccess.foreach {
          _(Json.parse(response.getResponseBody)
            .asOpt[PostEventResponse](PostEventResponse.readFormat)
            .getOrElse(PostEventResponse(None, None))
          )
        }
      case Left(ex) =>
        log.error(s"Failure while posting event", ex)
    }
  }

  @inline private[this] def apiUrl(action: String) =
    url(s"${apiEP.url}/$action").addQueryParameter("api_key", apiEP.apiKey)

  def timed[T](aspect: String, tags: String*)(block: => T)
  (implicit log: Logger): T = {
    val start = System.currentTimeMillis()
    val result = block
    val end = System.currentTimeMillis()
    recordExecutionTime(aspect, end - start, tags:_*)
    result
  }

  def recordExecutionTime(aspects: Seq[String], timeInMs: Long, tags: String*)
  (implicit log: Logger) {
    aspects.foreach { recordExecutionTime(_, timeInMs, tags:_*) }
  }

  def recordExecutionTime(aspect: String, timeInMs: Long, tags: String*)
  (implicit log: Logger) {
    log.debug(
      s"Recording execution time: $aspect took $timeInMs ms [tags: $tags]"
    )
    stats.recordExecutionTime(aspect, timeInMs, tags:_*)
  }

  def counter(aspect: String, delta: Int, tags: Seq[String])
  (implicit log: Logger) {
    log.debug(
      s"Recording counter change: $aspect delta $delta [tags: $tags]"
    )
    stats.count(aspect, delta, tags:_*)
  }
}