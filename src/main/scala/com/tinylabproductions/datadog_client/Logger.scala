package com.tinylabproductions.datadog_client

/**
 * Created with IntelliJ IDEA.
 * User: arturas
 * Date: 6/19/13
 * Time: 3:20 PM
 * To change this template use File | Settings | File Templates.
 */
trait Logger {
  def error(msg: => String)
  def error(msg: => String, ex: => Throwable)
  def info(msg: => String)
  def debug(msg: => String)
}

object NullLogger extends Logger {
  def error(msg: => String) {}
  def error(msg: => String, ex: => Throwable) {}
  def info(msg: => String) {}
  def debug(msg: => String) {}
}