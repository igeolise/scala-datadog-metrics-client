import sbt._
import sbt.Keys._

object DatadogMetricsClientBuild extends Build {
  lazy val datadogMetricsClient = Project(
    id = "datadog-metrics-client",
    base = file("."),
    settings = Project.defaultSettings ++ Seq(
      name := "DataDog metrics client",
      organization := "com.tinylabproductions",
      version := "1.0",
      scalaVersion := "2.10.3",
      scalaBinaryVersion := "2.10",
      resolvers ++= Seq(
        "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/"
      ),
      libraryDependencies ++= Seq(
        "com.indeed" % "java-dogstatsd-client" % "2.0.7",
        "com.typesafe.play"       %% "play-json" % "2.2.1",
        // Using older than 0.10.1 here, because new one requires newer netty
        // and that produces errors with play 2.1.0/2.1.1.
        "net.databinder.dispatch" %% "dispatch-core" % "0.9.5",

        "org.scalautils" % "scalautils_2.10" % "2.0",
        "org.scalatest" %% "scalatest" % "2.0" % "test"
      )
    )
  )
}
